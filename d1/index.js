//Query Operators


//Comparison Operators

//$gt / $gte Operator
//db.collectionName.find({ field: { $gt: value } })
db.users.find({ age: { $gt: 76 } })

//$lt / $lte Operator
db.users.find({ age: { $lt: 76 } })

//Not equal operator ($ne)
//db.collectionName.find({ field: { $ne: value } })

db.users.find({ age: { $ne: 76 } })


//$in operator
//It allows to find documents with specific match criteria 
//db.collectionName.find({ field: { $in: value } })
db.users.find({ courses: { $in: ["Sass", "React"] } })


//Logical Query Operator
//or
//db.collectionName.find({ $or: [{ fieldA: valueA }, { fieldB: valueB }] })
db.users.find({ $or: [ { firstName: "Neil" }, { age: { $gt: 30 } } ] })

//and
//db.collectionName.find({ $and: [{ fieldA: valueA }, { fieldB: valueB }] })
db.users.find({ $and: [ { age: { $ne: 82 } }, { age: { $ne: 76 } } ] })


//Evaluation Query Operator
//$regex operator -> used to look for a partial match in a given field
//it is used to search for STRINGS in collection


//db.collectionName.find({ field: {$regex: 'pattern/keyword', $options: '$i' })
//case sensitive
db.users.find({ firstName: {$regex: 'N' } })

//$options with '$i' parameter specifies that we want to carry out search without considering the upper case and lower cases.
